<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230611111301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE steps (id INT AUTO_INCREMENT NOT NULL, secondary_category_id INT NOT NULL, category_id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_34220A72EA0D7566 (secondary_category_id), INDEX IDX_34220A7212469DE2 (category_id), INDEX IDX_34220A72A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE steps ADD CONSTRAINT FK_34220A72EA0D7566 FOREIGN KEY (secondary_category_id) REFERENCES secondary_category (id)');
        $this->addSql('ALTER TABLE steps ADD CONSTRAINT FK_34220A7212469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE steps ADD CONSTRAINT FK_34220A72A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE steps DROP FOREIGN KEY FK_34220A72EA0D7566');
        $this->addSql('ALTER TABLE steps DROP FOREIGN KEY FK_34220A7212469DE2');
        $this->addSql('ALTER TABLE steps DROP FOREIGN KEY FK_34220A72A76ED395');
        $this->addSql('DROP TABLE steps');
    }
}
