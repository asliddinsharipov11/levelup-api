<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230611140905 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE question_line (id INT AUTO_INCREMENT NOT NULL, question_category_id INT NOT NULL, steps_id INT NOT NULL, user_id INT NOT NULL, en LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', uz LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', en_sort LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_8FFA5919F142426F (question_category_id), INDEX IDX_8FFA59191EBBD054 (steps_id), INDEX IDX_8FFA5919A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question_line ADD CONSTRAINT FK_8FFA5919F142426F FOREIGN KEY (question_category_id) REFERENCES question_category (id)');
        $this->addSql('ALTER TABLE question_line ADD CONSTRAINT FK_8FFA59191EBBD054 FOREIGN KEY (steps_id) REFERENCES steps (id)');
        $this->addSql('ALTER TABLE question_line ADD CONSTRAINT FK_8FFA5919A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE question_line DROP FOREIGN KEY FK_8FFA5919F142426F');
        $this->addSql('ALTER TABLE question_line DROP FOREIGN KEY FK_8FFA59191EBBD054');
        $this->addSql('ALTER TABLE question_line DROP FOREIGN KEY FK_8FFA5919A76ED395');
        $this->addSql('DROP TABLE question_line');
    }
}
