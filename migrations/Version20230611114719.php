<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230611114719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vocabulary (id INT AUTO_INCREMENT NOT NULL, steps_id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, accent VARCHAR(255) NOT NULL, en VARCHAR(255) NOT NULL, uz VARCHAR(255) NOT NULL, en_text VARCHAR(255) NOT NULL, uz_text VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_9099C97B1EBBD054 (steps_id), INDEX IDX_9099C97BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vocabulary ADD CONSTRAINT FK_9099C97B1EBBD054 FOREIGN KEY (steps_id) REFERENCES steps (id)');
        $this->addSql('ALTER TABLE vocabulary ADD CONSTRAINT FK_9099C97BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vocabulary DROP FOREIGN KEY FK_9099C97B1EBBD054');
        $this->addSql('ALTER TABLE vocabulary DROP FOREIGN KEY FK_9099C97BA76ED395');
        $this->addSql('DROP TABLE vocabulary');
    }
}
