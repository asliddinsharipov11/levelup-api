<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Component\User\Dtos\RefreshTokenRequestDto;
use App\Controller\DeleteAction;
use App\Controller\UserAboutMeAction;
use App\Controller\UserAuthAction;
use App\Controller\UserAuthByRefreshTokenAction;
use App\Controller\UserChangePasswordAction;
use App\Controller\UserCreateAction;
use App\Controller\UserIsUniqueEmailAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Repository\UserRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        'get'                => [
            'security'              => "is_granted('ROLE_ADMIN')",
            'normalization_context' => ['groups' => ['users:read']],
        ],
        'post'               => [
            'controller' => UserCreateAction::class,
        ],
        'aboutMe'            => [
            'controller'      => UserAboutMeAction::class,
            'method'          => 'get',
            'path'            => 'users/about_me',
            'openapi_context' => [
                'summary'    => 'Shows info about the authenticated user',
//                'parameters' => [
//                    [
//                        'in'       => 'query',
//                        'name'     => 'test',
//                        'type'     => 'string',
//                        'required' => true,
//                        'example'  => '{"id": 1, "hash": "e9151ae9de2d5a3cd1d16834431a0317"}',
//                    ],
//                ],
            ],
        ],
        'auth'               => [
            'controller'      => UserAuthAction::class,
            'method'          => 'post',
            'path'            => 'users/auth',
            'openapi_context' => ['summary' => 'Authorization'],
        ],
        'authByRefreshToken' => [
            'controller'      => UserAuthByRefreshTokenAction::class,
            'method'          => 'post',
            'path'            => 'users/auth/refreshToken',
            'openapi_context' => ['summary' => 'Authorization by refreshToken'],
            'input'           => RefreshTokenRequestDto::class,
        ],
        'isUniqueEmail'      => [
            'controller'              => UserIsUniqueEmailAction::class,
            'method'                  => 'post',
            'path'                    => 'users/is_unique_email',
            'openapi_context'         => ['summary' => 'Checks email for uniqueness'],
            'denormalization_context' => ['groups' => ['user:isUniqueEmail:write']],
        ],
    ],
    itemOperations: [
        'get'            => [
            'security' => "object == user || is_granted('ROLE_ADMIN')",
        ],
        'put'            => [
            'security'                => "object == user || is_granted('ROLE_ADMIN')",
            'denormalization_context' => ['groups' => ['user:put:write']],
        ],
        'delete'         => [
            'controller' => DeleteAction::class,
            'security'   => "object == user || is_granted('ROLE_ADMIN')",
        ],
        'changePassword' => [
            'controller'              => UserChangePasswordAction::class,
            'method'                  => 'put',
            'path'                    => 'users/{id}/password',
            'security'                => "object == user || is_granted('ROLE_ADMIN')",
            'openapi_context'         => ['summary' => 'Changes password'],
            'denormalization_context' => ['groups' => ['user:changePassword:write']],
        ],
    ],
    denormalizationContext: ['groups' => ['user:write']],
    normalizationContext: ['groups' => ['user:read', 'users:read']],
)]
#[ApiFilter(OrderFilter::class, properties: ['id', 'createdAt', 'updatedAt', 'email'])]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'email' => 'partial'])]
#[UniqueEntity('email', message: 'This email is already used')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements
    UserInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface,
    PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['users:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    #[Groups(['users:read', 'user:write', 'user:put:write', 'user:isUniqueEmail:write'])]
    private $email;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['user:write', 'user:changePassword:write'])]
    private $password;

    #[ORM\Column(type: 'array')]
    #[Groups(['user:read'])]
    private $roles = [];

    #[ORM\Column(type: 'datetime')]
    #[Groups(['user:read'])]
    private $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['user:read'])]
    private $updatedAt;

    #[ORM\Column(type: 'boolean')]
    private $isDeleted = false;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Category::class)]
    private Collection $categories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: SecondaryCategory::class)]
    private Collection $secondaryCategories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Steps::class)]
    private Collection $steps;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Vocabulary::class)]
    private Collection $vocabularies;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: QuestionCategory::class)]
    private Collection $questionCategories;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: QuestionTest::class)]
    private Collection $questionTests;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: QuestionFull::class)]
    private Collection $questionFulls;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: QuestionLine::class)]
    private Collection $questionLines;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: QuestionFind::class)]
    private Collection $questionFinds;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: SecondaryUser::class)]
    private Collection $secondaryUsers;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->secondaryCategories = new ArrayCollection();
        $this->steps = new ArrayCollection();
        $this->vocabularies = new ArrayCollection();
        $this->questionCategories = new ArrayCollection();
        $this->questionTests = new ArrayCollection();
        $this->questionFulls = new ArrayCollection();
        $this->questionLines = new ArrayCollection();
        $this->questionFinds = new ArrayCollection();
        $this->secondaryUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(string $role): self
    {
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function deleteRole(string $role): self
    {
        $roles = $this->roles;

        foreach ($roles as $roleKey => $roleName) {
            if ($roleName === $role) {
                unset($roles[$roleKey]);
                $this->setRoles($roles);
            }
        }

        return $this;
    }

    public function getSalt(): string
    {
        return '';
    }

    public function getUsername(): string
    {
        return $this->getEmail();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->getId();
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): static
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
            $category->setUser($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): static
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getUser() === $this) {
                $category->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SecondaryCategory>
     */
    public function getSecondaryCategories(): Collection
    {
        return $this->secondaryCategories;
    }

    public function addSecondaryCategory(SecondaryCategory $secondaryCategory): static
    {
        if (!$this->secondaryCategories->contains($secondaryCategory)) {
            $this->secondaryCategories->add($secondaryCategory);
            $secondaryCategory->setUser($this);
        }

        return $this;
    }

    public function removeSecondaryCategory(SecondaryCategory $secondaryCategory): static
    {
        if ($this->secondaryCategories->removeElement($secondaryCategory)) {
            // set the owning side to null (unless already changed)
            if ($secondaryCategory->getUser() === $this) {
                $secondaryCategory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Steps>
     */
    public function getSteps(): Collection
    {
        return $this->steps;
    }

    public function addStep(Steps $step): static
    {
        if (!$this->steps->contains($step)) {
            $this->steps->add($step);
            $step->setUser($this);
        }

        return $this;
    }

    public function removeStep(Steps $step): static
    {
        if ($this->steps->removeElement($step)) {
            // set the owning side to null (unless already changed)
            if ($step->getUser() === $this) {
                $step->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vocabulary>
     */
    public function getVocabularies(): Collection
    {
        return $this->vocabularies;
    }

    public function addVocabulary(Vocabulary $vocabulary): static
    {
        if (!$this->vocabularies->contains($vocabulary)) {
            $this->vocabularies->add($vocabulary);
            $vocabulary->setUser($this);
        }

        return $this;
    }

    public function removeVocabulary(Vocabulary $vocabulary): static
    {
        if ($this->vocabularies->removeElement($vocabulary)) {
            // set the owning side to null (unless already changed)
            if ($vocabulary->getUser() === $this) {
                $vocabulary->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionCategory>
     */
    public function getQuestionCategories(): Collection
    {
        return $this->questionCategories;
    }

    public function addQuestionCategory(QuestionCategory $questionCategory): static
    {
        if (!$this->questionCategories->contains($questionCategory)) {
            $this->questionCategories->add($questionCategory);
            $questionCategory->setUser($this);
        }

        return $this;
    }

    public function removeQuestionCategory(QuestionCategory $questionCategory): static
    {
        if ($this->questionCategories->removeElement($questionCategory)) {
            // set the owning side to null (unless already changed)
            if ($questionCategory->getUser() === $this) {
                $questionCategory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionTest>
     */
    public function getQuestionTests(): Collection
    {
        return $this->questionTests;
    }

    public function addQuestionTest(QuestionTest $questionTest): static
    {
        if (!$this->questionTests->contains($questionTest)) {
            $this->questionTests->add($questionTest);
            $questionTest->setUser($this);
        }

        return $this;
    }

    public function removeQuestionTest(QuestionTest $questionTest): static
    {
        if ($this->questionTests->removeElement($questionTest)) {
            // set the owning side to null (unless already changed)
            if ($questionTest->getUser() === $this) {
                $questionTest->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionFull>
     */
    public function getQuestionFulls(): Collection
    {
        return $this->questionFulls;
    }

    public function addQuestionFull(QuestionFull $questionFull): static
    {
        if (!$this->questionFulls->contains($questionFull)) {
            $this->questionFulls->add($questionFull);
            $questionFull->setUser($this);
        }

        return $this;
    }

    public function removeQuestionFull(QuestionFull $questionFull): static
    {
        if ($this->questionFulls->removeElement($questionFull)) {
            // set the owning side to null (unless already changed)
            if ($questionFull->getUser() === $this) {
                $questionFull->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionLine>
     */
    public function getQuestionLines(): Collection
    {
        return $this->questionLines;
    }

    public function addQuestionLine(QuestionLine $questionLine): static
    {
        if (!$this->questionLines->contains($questionLine)) {
            $this->questionLines->add($questionLine);
            $questionLine->setUser($this);
        }

        return $this;
    }

    public function removeQuestionLine(QuestionLine $questionLine): static
    {
        if ($this->questionLines->removeElement($questionLine)) {
            // set the owning side to null (unless already changed)
            if ($questionLine->getUser() === $this) {
                $questionLine->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionFind>
     */
    public function getQuestionFinds(): Collection
    {
        return $this->questionFinds;
    }

    public function addQuestionFind(QuestionFind $questionFind): static
    {
        if (!$this->questionFinds->contains($questionFind)) {
            $this->questionFinds->add($questionFind);
            $questionFind->setUser($this);
        }

        return $this;
    }

    public function removeQuestionFind(QuestionFind $questionFind): static
    {
        if ($this->questionFinds->removeElement($questionFind)) {
            // set the owning side to null (unless already changed)
            if ($questionFind->getUser() === $this) {
                $questionFind->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SecondaryUser>
     */
    public function getSecondaryUsers(): Collection
    {
        return $this->secondaryUsers;
    }

    public function addSecondaryUser(SecondaryUser $secondaryUser): static
    {
        if (!$this->secondaryUsers->contains($secondaryUser)) {
            $this->secondaryUsers->add($secondaryUser);
            $secondaryUser->setUser($this);
        }

        return $this;
    }

    public function removeSecondaryUser(SecondaryUser $secondaryUser): static
    {
        if ($this->secondaryUsers->removeElement($secondaryUser)) {
            // set the owning side to null (unless already changed)
            if ($secondaryUser->getUser() === $this) {
                $secondaryUser->setUser(null);
            }
        }

        return $this;
    }
}
