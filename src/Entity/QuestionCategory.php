<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\QuestionCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuestionCategoryRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post'
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['cat:write']],
    normalizationContext: ['groups' => ['cat:read']]
)]
class QuestionCategory implements
    UserSettableInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['cat:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['cat:read','cat:write'])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'questionCategories')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['cat:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['cat:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['cat:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    #[Groups(['cat:read'])]
    private ?bool $isDeleted = false;

    #[ORM\OneToMany(mappedBy: 'questionCategory', targetEntity: QuestionTest::class)]
    private Collection $questionTests;

    #[ORM\OneToMany(mappedBy: 'questionCategory', targetEntity: QuestionFull::class)]
    private Collection $questionFulls;

    #[ORM\OneToMany(mappedBy: 'questionCategory', targetEntity: QuestionLine::class)]
    private Collection $questionLines;

    #[ORM\OneToMany(mappedBy: 'questionCategory', targetEntity: QuestionFind::class)]
    private Collection $questionFinds;

    public function __construct()
    {
        $this->questionTests = new ArrayCollection();
        $this->questionFulls = new ArrayCollection();
        $this->questionLines = new ArrayCollection();
        $this->questionFinds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return Collection<int, QuestionTest>
     */
    public function getQuestionTests(): Collection
    {
        return $this->questionTests;
    }

    public function addQuestionTest(QuestionTest $questionTest): static
    {
        if (!$this->questionTests->contains($questionTest)) {
            $this->questionTests->add($questionTest);
            $questionTest->setQuestionCategory($this);
        }

        return $this;
    }

    public function removeQuestionTest(QuestionTest $questionTest): static
    {
        if ($this->questionTests->removeElement($questionTest)) {
            // set the owning side to null (unless already changed)
            if ($questionTest->getQuestionCategory() === $this) {
                $questionTest->setQuestionCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionFull>
     */
    public function getQuestionFulls(): Collection
    {
        return $this->questionFulls;
    }

    public function addQuestionFull(QuestionFull $questionFull): static
    {
        if (!$this->questionFulls->contains($questionFull)) {
            $this->questionFulls->add($questionFull);
            $questionFull->setQuestionCategory($this);
        }

        return $this;
    }

    public function removeQuestionFull(QuestionFull $questionFull): static
    {
        if ($this->questionFulls->removeElement($questionFull)) {
            // set the owning side to null (unless already changed)
            if ($questionFull->getQuestionCategory() === $this) {
                $questionFull->setQuestionCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionLine>
     */
    public function getQuestionLines(): Collection
    {
        return $this->questionLines;
    }

    public function addQuestionLine(QuestionLine $questionLine): static
    {
        if (!$this->questionLines->contains($questionLine)) {
            $this->questionLines->add($questionLine);
            $questionLine->setQuestionCategory($this);
        }

        return $this;
    }

    public function removeQuestionLine(QuestionLine $questionLine): static
    {
        if ($this->questionLines->removeElement($questionLine)) {
            // set the owning side to null (unless already changed)
            if ($questionLine->getQuestionCategory() === $this) {
                $questionLine->setQuestionCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionFind>
     */
    public function getQuestionFinds(): Collection
    {
        return $this->questionFinds;
    }

    public function addQuestionFind(QuestionFind $questionFind): static
    {
        if (!$this->questionFinds->contains($questionFind)) {
            $this->questionFinds->add($questionFind);
            $questionFind->setQuestionCategory($this);
        }

        return $this;
    }

    public function removeQuestionFind(QuestionFind $questionFind): static
    {
        if ($this->questionFinds->removeElement($questionFind)) {
            // set the owning side to null (unless already changed)
            if ($questionFind->getQuestionCategory() === $this) {
                $questionFind->setQuestionCategory(null);
            }
        }

        return $this;
    }
}
