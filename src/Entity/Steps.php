<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\StepsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: StepsRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post'
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['step:write','category:write']],
    normalizationContext: ['groups' => ['step:read','category:read']]
)]
#[ApiFilter(OrderFilter::class,properties:['id'])]
#[ApiFilter(SearchFilter::class,properties:['category'=>'exact','secondaryCategory'=>'exact'])]
class Steps implements
    UserSettableInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['step:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['step:read','step:write'])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'steps')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['step:read','step:write'])]
    private ?SecondaryCategory $secondaryCategory = null;

    #[ORM\ManyToOne(inversedBy: 'steps')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['step:read','step:write'])]
    private ?Category $category = null;

    #[ORM\ManyToOne(inversedBy: 'steps')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['step:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['step:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['step:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    #[Groups(['step:read'])]
    private ?bool $isDeleted = false;

    #[ORM\OneToMany(mappedBy: 'step', targetEntity: Vocabulary::class)]
    private Collection $vocabularies;

    #[ORM\OneToMany(mappedBy: 'steps', targetEntity: QuestionTest::class)]
    private Collection $questionTests;

    #[ORM\OneToMany(mappedBy: 'steps', targetEntity: QuestionFull::class)]
    private Collection $questionFulls;

    #[ORM\OneToMany(mappedBy: 'steps', targetEntity: QuestionLine::class)]
    private Collection $questionLines;

    #[ORM\OneToMany(mappedBy: 'steps', targetEntity: QuestionFind::class)]
    private Collection $questionFinds;

    public function __construct()
    {
        $this->vocabularies = new ArrayCollection();
        $this->questionTests = new ArrayCollection();
        $this->questionFulls = new ArrayCollection();
        $this->questionLines = new ArrayCollection();
        $this->questionFinds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSecondaryCategory(): ?SecondaryCategory
    {
        return $this->secondaryCategory;
    }

    public function setSecondaryCategory(?SecondaryCategory $secondaryCategory): static
    {
        $this->secondaryCategory = $secondaryCategory;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return Collection<int, Vocabulary>
     */
    public function getVocabularies(): Collection
    {
        return $this->vocabularies;
    }

    public function addVocabulary(Vocabulary $vocabulary): static
    {
        if (!$this->vocabularies->contains($vocabulary)) {
            $this->vocabularies->add($vocabulary);
            $vocabulary->setSteps($this);
        }

        return $this;
    }

    public function removeVocabulary(Vocabulary $vocabulary): static
    {
        if ($this->vocabularies->removeElement($vocabulary)) {
            // set the owning side to null (unless already changed)
            if ($vocabulary->getSteps() === $this) {
                $vocabulary->setSteps(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionTest>
     */
    public function getQuestionTests(): Collection
    {
        return $this->questionTests;
    }

    public function addQuestinTest(QuestionTest $questionTest): static
    {
        if (!$this->questionTests->contains($questionTest)) {
            $this->questionTests->add($questionTest);
            $questionTest->setSteps($this);
        }

        return $this;
    }

    public function removeQuestinTest(QuestionTest $questionTest): static
    {
        if ($this->questionTests->removeElement($questionTest)) {
            // set the owning side to null (unless already changed)
            if ($questionTest->getSteps() === $this) {
                $questionTest->setSteps(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionFull>
     */
    public function getQuestionFulls(): Collection
    {
        return $this->questionFulls;
    }

    public function addQuestionFull(QuestionFull $questionFull): static
    {
        if (!$this->questionFulls->contains($questionFull)) {
            $this->questionFulls->add($questionFull);
            $questionFull->setSteps($this);
        }

        return $this;
    }

    public function removeQuestionFull(QuestionFull $questionFull): static
    {
        if ($this->questionFulls->removeElement($questionFull)) {
            // set the owning side to null (unless already changed)
            if ($questionFull->getSteps() === $this) {
                $questionFull->setSteps(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionLine>
     */
    public function getQuestionLines(): Collection
    {
        return $this->questionLines;
    }

    public function addQuestionLine(QuestionLine $questionLine): static
    {
        if (!$this->questionLines->contains($questionLine)) {
            $this->questionLines->add($questionLine);
            $questionLine->setSteps($this);
        }

        return $this;
    }

    public function removeQuestionLine(QuestionLine $questionLine): static
    {
        if ($this->questionLines->removeElement($questionLine)) {
            // set the owning side to null (unless already changed)
            if ($questionLine->getSteps() === $this) {
                $questionLine->setSteps(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionFind>
     */
    public function getQuestionFinds(): Collection
    {
        return $this->questionFinds;
    }

    public function addQuestionFind(QuestionFind $questionFind): static
    {
        if (!$this->questionFinds->contains($questionFind)) {
            $this->questionFinds->add($questionFind);
            $questionFind->setSteps($this);
        }

        return $this;
    }

    public function removeQuestionFind(QuestionFind $questionFind): static
    {
        if ($this->questionFinds->removeElement($questionFind)) {
            // set the owning side to null (unless already changed)
            if ($questionFind->getSteps() === $this) {
                $questionFind->setSteps(null);
            }
        }

        return $this;
    }
}
