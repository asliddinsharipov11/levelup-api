<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post'
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['category:write']],
    normalizationContext: ['groups' => ['category:read']]
)]
#[ApiFilter(OrderFilter::class,properties:['id'])]
#[ApiFilter(SearchFilter::class,properties:['category'=>'exact'])]
class Category implements 
    UserSettableInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['category:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
     #[Groups(['category:read','category:write'])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'categories')]
    #[ORM\JoinColumn(nullable: false)]
     #[Groups(['category:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
     #[Groups(['category:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
     #[Groups(['category:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
     #[Groups(['category:read'])]
    private ?bool $isDeleted = false;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: SecondaryCategory::class)]
    private Collection $secondaryCategories;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Steps::class)]
    private Collection $steps;

    public function __construct()
    {
        $this->secondaryCategories = new ArrayCollection();
        $this->steps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return Collection<int, SecondaryCategory>
     */
    public function getSecondaryCategories(): Collection
    {
        return $this->secondaryCategories;
    }

    public function addSecondaryCategory(SecondaryCategory $secondaryCategory): static
    {
        if (!$this->secondaryCategories->contains($secondaryCategory)) {
            $this->secondaryCategories->add($secondaryCategory);
            $secondaryCategory->setCategory($this);
        }

        return $this;
    }

    public function removeSecondaryCategory(SecondaryCategory $secondaryCategory): static
    {
        if ($this->secondaryCategories->removeElement($secondaryCategory)) {
            // set the owning side to null (unless already changed)
            if ($secondaryCategory->getCategory() === $this) {
                $secondaryCategory->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Steps>
     */
    public function getSteps(): Collection
    {
        return $this->steps;
    }

    public function addStep(Steps $step): static
    {
        if (!$this->steps->contains($step)) {
            $this->steps->add($step);
            $step->setCategory($this);
        }

        return $this;
    }

    public function removeStep(Steps $step): static
    {
        if ($this->steps->removeElement($step)) {
            // set the owning side to null (unless already changed)
            if ($step->getCategory() === $this) {
                $step->setCategory(null);
            }
        }

        return $this;
    }
}
