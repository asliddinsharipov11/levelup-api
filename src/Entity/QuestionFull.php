<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\DeleteAction;
use App\Controller\QuestionFull\FullImgCreateAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\QuestionFullRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuestionFullRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'img'       => [
            'controller'    => FullImgCreateAction::class,
            'method'     => 'post',
            'path'       => 'question_fulls/img',
            'deserialize' => false,
            'validation_groups' => ['Default', 'media_object_create'],
            'openapi_context' => [
                'summary'    => 'Upload img',
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                    'id' => [
                                        'type' => 'string'
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
            ],

        ]
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['full:write']],
    normalizationContext: ['groups' => ['full:read']]
)]
#[ApiFilter(OrderFilter::class,properties:['id'])]
#[ApiFilter(SearchFilter::class,properties:['steps'=>'exact','questionCategory'=>'exact','id'=>'exact'])]
class QuestionFull implements
    UserSettableInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['full:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['full:read','full:write'])]
    private ?string $en = null;

    #[ORM\Column(length: 255)]
    #[Groups(['full:read','full:write'])]
    private ?string $uz = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['full:read'])]
    private ?string $img = null;

    #[ORM\ManyToOne(inversedBy: 'questionFulls')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['full:read','full:write'])]
    private ?QuestionCategory $questionCategory = null;

    #[ORM\ManyToOne(inversedBy: 'questionFulls')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['full:read','full:write'])]
    private ?Steps $steps = null;

    #[ORM\ManyToOne(inversedBy: 'questionFulls')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['full:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['full:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['full:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    #[Groups(['full:read'])]
    private ?bool $isDeleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEn(): ?string
    {
        return $this->en;
    }

    public function setEn(string $en): static
    {
        $this->en = $en;

        return $this;
    }

    public function getUz(): ?string
    {
        return $this->uz;
    }

    public function setUz(string $uz): static
    {
        $this->uz = $uz;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): static
    {
        $this->img = $img;

        return $this;
    }

    public function getQuestionCategory(): ?QuestionCategory
    {
        return $this->questionCategory;
    }

    public function setQuestionCategory(?QuestionCategory $questionCategory): static
    {
        $this->questionCategory = $questionCategory;

        return $this;
    }

    public function getSteps(): ?Steps
    {
        return $this->steps;
    }

    public function setSteps(?Steps $steps): static
    {
        $this->steps = $steps;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
