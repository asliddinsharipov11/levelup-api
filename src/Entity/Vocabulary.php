<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\DeleteAction;
use App\Controller\Vocabulary\VocabularyImgCreateAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\VocabularyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: VocabularyRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'img'       => [
            'controller'    => VocabularyImgCreateAction::class,
            'method'     => 'post',
            'path'       => 'vocabularies/img',
            'deserialize' => false,
            'validation_groups' => ['Default', 'media_object_create'],
            'openapi_context' => [
                'summary'    => 'Upload img',
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                    'id' => [
                                        'type' => 'string'
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
            ],

        ]
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['vacub:write']],
    normalizationContext: ['groups' => ['vacub:read']]
)]
#[ApiFilter(OrderFilter::class,properties:['id'])]
#[ApiFilter(SearchFilter::class,properties:['steps'=>'exact'])]
class Vocabulary implements
    CreatedAtSettableInterface,
    UserSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['vacub:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $accent = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $en = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $uz = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $enText = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?string $uzText = null;

    #[ORM\ManyToOne(inversedBy: 'vocabularies')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['vacub:read','vacub:write'])]
    private ?Steps $steps = null;

    #[ORM\ManyToOne(inversedBy: 'vocabularies')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['vacub:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['vacub:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['vacub:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    #[Groups(['vacub:read'])]
    private ?bool $isDeleted = false;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['vacub:read'])]
    private ?string $img = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getAccent(): ?string
    {
        return $this->accent;
    }

    public function setAccent(string $accent): static
    {
        $this->accent = $accent;

        return $this;
    }

    public function getEn(): ?string
    {
        return $this->en;
    }

    public function setEn(string $en): static
    {
        $this->en = $en;

        return $this;
    }

    public function getUz(): ?string
    {
        return $this->uz;
    }

    public function setUz(string $uz): static
    {
        $this->uz = $uz;

        return $this;
    }

    public function getEnText(): ?string
    {
        return $this->enText;
    }

    public function setEnText(string $enText): static
    {
        $this->enText = $enText;

        return $this;
    }

    public function getUzText(): ?string
    {
        return $this->uzText;
    }

    public function setUzText(string $uzText): static
    {
        $this->uzText = $uzText;

        return $this;
    }

    public function getSteps(): ?Steps
    {
        return $this->steps;
    }

    public function setSteps(?Steps $steps): static
    {
        $this->steps = $steps;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): static
    {
        $this->img = $img;

        return $this;
    }
}
