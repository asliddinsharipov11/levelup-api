<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Component\SecondaryUser\Dtos\SecondaryRefreshTokenRequestDto;
use App\Component\SecondaryUser\Dtos\SecondaryUserDto;
use App\Controller\DeleteAction;
use App\Controller\SecondaryUser\SecondaryUserAboutMeAction;
use App\Controller\SecondaryUser\SecondaryUserAuthAction;
use App\Controller\SecondaryUser\SecondaryUserAuthByRefreshTokenAction;
use App\Controller\SecondaryUser\SecondaryUserCreateAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\SecondaryUserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SecondaryUserRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post'      => [
            'controller'=> SecondaryUserCreateAction::class
        ],
        'aboutMe'            => [
            'controller'      => SecondaryUserAboutMeAction::class,
            'method'          => 'get',
            'path'            => 'secondary_users/about_me',
            'openapi_context' => [
                'summary'    => 'Shows info about the authenticated user',
//                'parameters' => [
//                    [
//                        'in'       => 'query',
//                        'name'     => 'test',
//                        'type'     => 'string',
//                        'required' => true,
//                        'example'  => '{"id": 1, "hash": "e9151ae9de2d5a3cd1d16834431a0317"}',
//                    ],
//                ],
            ],
        ],
        'auth'      => [
            'controller'    => SecondaryUserAuthAction::class,
            'input'         => SecondaryUserDto::class,
            'method'          => 'post',
            'path'            => 'secondary_users/auth',
            'openapi_context' => ['summary' => 'Authorization'],

        ],
        'authByRefreshToken' => [
            'controller'      => SecondaryUserAuthByRefreshTokenAction::class,
            'method'          => 'post',
            'path'            => 'secondary_users/auth/refreshToken',
            'openapi_context' => ['summary' => 'Authorization by refreshToken'],
            'input'           => SecondaryRefreshTokenRequestDto::class,
        ]
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user || is_granted('ROLE_ADMIN') || object == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user || is_granted('ROLE_ADMIN') || object == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user || is_granted('ROLE_ADMIN') || object == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['user2:write']],
    normalizationContext: ['groups' => ['user2:read']]
)]
#[ApiFilter(OrderFilter::class,properties: ['id','createdAt','updatedAt','name'])]
#[ApiFilter(SearchFilter::class,properties: ['id'=>'exact','email'=>'partial','user'=>'exact'])]
#[UniqueEntity('email', message: 'This email is already used')]
class SecondaryUser implements
    UserInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
   // UserSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['user2:read'])]
    private ?int $id = null;

    #[Assert\Email]
    #[ORM\Column(length: 255)]
    #[Groups(['user2:read','user2:write'])]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Groups(['user2:read','user2:write'])]
    private ?string $password = null;

    #[ORM\Column(type: Types::ARRAY)]
    #[Groups(['user2:read'])]
    private array $roles = [];

    #[ORM\ManyToOne(inversedBy: 'secondaryUsers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['user2:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['user2:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['user2:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    #[Groups(['user2:read'])]
    private ?bool $isDeleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER_2';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(string $role): self
    {
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function deleteRole(string $role): self
    {
        $roles = $this->roles;

        foreach ($roles as $roleKey => $roleName) {
            if ($roleName === $role) {
                unset($roles[$roleKey]);
                $this->setRoles($roles);
            }
        }

        return $this;
    }

    public function getSalt(): string
    {
        return '';
    }

    public function getUsername(): string
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->getId();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
