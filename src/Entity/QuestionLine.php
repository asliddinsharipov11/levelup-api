<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Entity\Interfaces\UserSettableInterface;
use App\Repository\QuestionLineRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuestionLineRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post'
    ],
    itemOperations: [
        'get'       => [
            'security'  => "object.getUser() == user"
        ],
        'put'       => [
            'security'  => "object.getUser() == user"
        ],
        'delete'    => [
            'security'  => "object.getUser() == user",
            'controller'=> DeleteAction::class
        ]
    ],
    denormalizationContext: ['groups' => ['line:write']],
    normalizationContext: ['groups' => ['line:read']]
)]
#[ApiFilter(OrderFilter::class,properties:['id'])]
#[ApiFilter(SearchFilter::class,properties:['steps'=>'exact','questionCategory'=>'exact','id'=>'exact'])]
class QuestionLine implements
    UserSettableInterface,
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['line:read'])]
    private ?int $id = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    #[Groups(['line:read','line:write'])]
    private array $en = [];

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    #[Groups(['line:read','line:write'])]
    private array $uz = [];

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    #[Groups(['line:read','line:write'])]
    private array $enSort = [];

    #[ORM\ManyToOne(inversedBy: 'questionLines')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['line:read','line:write'])]
    private ?QuestionCategory $questionCategory = null;

    #[ORM\ManyToOne(inversedBy: 'questionLines')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['line:read','line:write'])]
    private ?Steps $steps = null;

    #[ORM\ManyToOne(inversedBy: 'questionLines')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['line:read'])]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['line:read'])]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['line:read'])]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column]
    #[Groups(['line:read'])]
    private ?bool $isDeleted = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEn(): array
    {
        return $this->en;
    }

    public function setEn(?array $en): static
    {
        $this->en = $en;

        return $this;
    }

    public function getUz(): array
    {
        return $this->uz;
    }

    public function setUz(?array $uz): static
    {
        $this->uz = $uz;

        return $this;
    }

    public function getEnSort(): array
    {
        return $this->enSort;
    }

    public function setEnSort(?array $enSort): static
    {
        $this->enSort = $enSort;

        return $this;
    }

    public function getQuestionCategory(): ?QuestionCategory
    {
        return $this->questionCategory;
    }

    public function setQuestionCategory(?QuestionCategory $questionCategory): static
    {
        $this->questionCategory = $questionCategory;

        return $this;
    }

    public function getSteps(): ?Steps
    {
        return $this->steps;
    }

    public function setSteps(?Steps $steps): static
    {
        $this->steps = $steps;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): static
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
