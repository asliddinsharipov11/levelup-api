<?php declare(strict_types=1);

namespace App\Controller\SecondaryUser;

use App\Controller\Base\AbstractController;
use App\Entity\SecondaryUser;
use Symfony\Component\Security\Core\Security;

class SecondaryUserAboutMeAction extends AbstractController
{
    public function __invoke(
        Security $security
    ):SecondaryUser
    {
        return $security->getUser();
    }
}