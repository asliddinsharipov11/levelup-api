<?php declare(strict_types=1);

namespace App\Controller\SecondaryUser;

use App\Component\SecondaryUser\Dtos\SecondaryUserDto;
use App\Component\SecondaryUser\PasswordHasher;
use App\Component\SecondaryUser\SecondaryTokenCreator;
use App\Component\SecondaryUser\SecondaryUserMaker;
use App\Controller\Base\AbstractController;
use App\Controller\Base\Constants\ResponseFormat;
use App\Entity\SecondaryUser;
use App\Repository\SecondaryUserRepository;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SecondaryUserCreateAction extends AbstractController
{
    public function __invoke(
        Request $request,
        SecondaryUserMaker $maker,
        SecondaryTokenCreator $tokensCreator,
        
    ):Response
    {
        $dto = $this->getDtoFromRequest($request,SecondaryUserDto::class);

        $this->validate($dto);

        try{
            
            return $this->responseNormalized($tokensCreator->create($maker->make($dto)), Response::HTTP_OK, ResponseFormat::JSON);
        }catch(Exception $e){
            throw new BadRequestHttpException($e->getMessage());
        }
        
    }
}