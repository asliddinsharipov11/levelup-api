<?php declare(strict_types=1);

namespace App\Controller\SecondaryUser;

use App\Component\SecondaryUser\Dtos\SecondaryUserDto;
use App\Component\SecondaryUser\PasswordHasher;
use App\Component\SecondaryUser\SecondaryTokenCreator;
use App\Component\User\Exceptions\AuthException;
use App\Controller\Base\AbstractController;
use App\Controller\Base\Constants\ResponseFormat;
use App\Repository\SecondaryUserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SecondaryUserAuthAction extends AbstractController
{
    public function __invoke(
        Request $request,
        PasswordHasher $passwordHasher,
        SecondaryTokenCreator $tokensCreator,
        SecondaryUserRepository $secondaryUserRepository
    ):Response
    {
        $dto = $this->getDtoFromRequest($request,SecondaryUserDto::class);

        $u = true;
        $this->validate($dto);
        $user = $secondaryUserRepository->findOneBy(['email' => $dto->getEmail()]);

        if ($user === null) {
            $this->throwInvalidCredentials();
        }

        if($user){
            if($passwordHasher->isPasswordValid($user->getPassword(),$dto->getPassword())){
                $u = false;
                return $this->responseNormalized($tokensCreator->create($user), Response::HTTP_OK, ResponseFormat::JSON);
            }
        }        
        
    }

     /**
     * @throws AuthException
     */
    private function throwInvalidCredentials(): void
    {
        throw new AuthException('Invalid credentials');
    }

}