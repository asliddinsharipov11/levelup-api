<?php declare(strict_types=1);

namespace App\Controller\QuestionFull;

use App\Component\QuestionFull\FullImgUpload;
use App\Controller\Base\AbstractController;
use Exception;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FullImgCreateAction extends AbstractController
{
    public function __invoke(
        Request $request,
        FullImgUpload $fullImgUpload
    )
    {
        $file = $request->files->get('file');
        $id = $request->get('id');
        if(!$file && !$id){
            throw new BadRequestException('file is requarid');
        }

        try{
            return $fullImgUpload->update($file,intval($id));
        }catch(Exception $e){
            throw new BadRequestHttpException($e->getMessage());
        }


    }
}