<?php declare(strict_types=1);

namespace App\Component\QuestionFull;

use App\Component\Media\UploadFile;
use App\Entity\QuestionFull;


use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;

class FullImgUpload
{
    public function __construct(
        private ManagerRegistry $registry,
        private UploadFile $uploadFile,
        private Security $security
    )
    {}
    public function update(UploadedFile $file,int $id):QuestionFull
    {
        $entityManager = $this->registry->getManager();
        $data = $entityManager->getRepository(QuestionFull::class)->find($id);
        
        if(!$data){
            throw new BadRequestHttpException('Category not found');
        }
        if($this->security->getUser() == $data->getUser()){
            $data->setImg($this->uploadFile->upload($file));
            $entityManager->flush();
        }else{
            throw new BadRequestException('User not found',400);
        }
        
        return $data;
    }
}