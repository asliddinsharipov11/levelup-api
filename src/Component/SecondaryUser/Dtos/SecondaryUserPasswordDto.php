<?php declare(strict_types=1);

namespace App\Component\SecondaryUser\Dtos;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
class SecondaryUserPasswordDto
{
    

    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(min:4,max:4)]
        #[Groups('user2:write')]
        private string $password,
    )
    {}

    public function getPassword(): string
    {
        return $this->password;
    }
}
