<?php declare(strict_types=1);

namespace App\Component\SecondaryUser\Dtos;

use App\Entity\UserCategory;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
class SecondaryUserDto
{
    

    public function __construct(
        #[Assert\Email]
        #[Assert\NotBlank]
        #[Groups('user2:write')]
        private string $email,

        #[Assert\NotBlank]
        //#[Assert\Length(min:4,max:4)]
        #[Groups('user2:write')]
        private string $password,

        
        
    )
    {}

    public function getEmail(): string
    {
        return $this->email;
    }
    public function getPassword(): string
    {
        return $this->password;
    }

}
