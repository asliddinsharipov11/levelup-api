<?php declare(strict_types=1);
namespace App\Component\SecondaryUser\Dtos;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Class RefreshTokenDto
 *
 * @package App\Component\SecondaryUser\Dtos
 */
class SecondaryRefreshTokenRequestDto
{
    public function __construct(
        #[Groups(['user2:write'])]
        private string $refreshToken
    ) {
    }
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }
}