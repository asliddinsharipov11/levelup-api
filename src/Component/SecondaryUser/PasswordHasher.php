<?php declare(strict_types=1);

namespace App\Component\SecondaryUser;

class PasswordHasher
{
    public function hasher(string $password): string
    {
        $hash = password_hash($password,PASSWORD_DEFAULT);

        return $hash;
    }

    public function isPasswordValid(string $hash,string $password ): bool
    {
        return password_verify( $password, $hash);
    }

}
