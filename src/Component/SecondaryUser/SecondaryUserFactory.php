<?php declare(strict_types=1);

namespace App\Component\SecondaryUser;

use App\Component\SecondaryUser\Dtos\SecondaryUserDto;
use App\Entity\SecondaryUser;
use App\Entity\User;
use DateTime;

class SecondaryUserFactory
{
    public function create(SecondaryUserDto $dto,string $hashPassword,User $user): SecondaryUser
    {
        $secondary = new SecondaryUser();

        $secondary->setEmail($dto->getEmail());
        $secondary->setPassword($hashPassword);
        $secondary->setCreatedAt(new DateTime());
        $secondary->setUser($user);

        return $secondary;

    }
}
