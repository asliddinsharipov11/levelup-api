<?php declare(strict_types=1);

namespace App\Component\SecondaryUser;

use App\Component\Manager\SaveManager;
use App\Component\SecondaryUser\Dtos\SecondaryUserDto;
use App\Entity\SecondaryUser;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;

class SecondaryUserMaker
{

    public function __construct(
        private SecondaryUserFactory $factory,
        private SaveManager $manager,
        private PasswordHasher $passwordHasher,
        private UserRepository $userRepository
    )
    {}
        

    public function make(SecondaryUserDto $dto): SecondaryUser
    {
        $secondary = $this->factory->create($dto,$this->passwordHasher->hasher($dto->getPassword()),$this->userRepository->find(1));
        $this->manager->save($secondary,true);

        return $secondary;
    }
}
