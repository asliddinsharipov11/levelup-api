<?php declare(strict_types=1);

namespace App\Component\Manager;

use App\Component\Core\AbstractManager;
use Doctrine\ORM\EntityManagerInterface;

class SaveManager extends AbstractManager
{
    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        parent::__construct($entityManager);
    }
}
